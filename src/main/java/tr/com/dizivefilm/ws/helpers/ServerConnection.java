package tr.com.dizivefilm.ws.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by ozcan on 15.10.2015.
 */
public class ServerConnection {

    static final Logger LOG = LoggerFactory.getLogger(ServerConnection.class);
    static final String mysqlUrl = "jdbc:mysql://localhost:3306/dvftmasterdb";
    static final String mysqlUser = "root";
    static final String mysqlPassword = "ozcan1453";
    private Connection connection = null;

    public Connection getMySqlConnection() {
        LOG.info("Getting mysql driver");

        try {
            Class.forName("com.mysql.jdbc.Driver");
            LOG.info("Driver found!");
            connection = DriverManager.getConnection(mysqlUrl, mysqlUser, mysqlPassword);
            LOG.info("Connected to server!");
        } catch (Exception e) {
            LOG.info(e.getMessage());
        }

        return connection;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException sqlex) {
        } catch (Exception ex) {
        }
    }

}
