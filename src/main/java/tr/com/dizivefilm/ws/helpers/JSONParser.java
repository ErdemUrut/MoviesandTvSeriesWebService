package tr.com.dizivefilm.ws.helpers;/*
 * Seri Bilgi Teknolojileri Destek Hizmetleri ve Ticaret Ltd. Şti. ("SERİ")
 * CONFIDENTIAL Unpublished Copyright (c) 2002-2014 SERİ, All Rights
 * Reserved.
 */

/**
 * JSON Parser
 * <p/>
 * Aşağıdaki adresten alındı;
 * https://gist.github.com/dmnugent80/b2e22e5546c4b1c391ee
 * <p/>
 * Açıklama;
 * http://danielnugent.blogspot.com.tr/2015/06/updated-jsonparser-with.html
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.HashMap;

@SuppressWarnings("JpaQueryApiInspection")
public class JSONParser {

    static final Logger LOG = LoggerFactory.getLogger(JSONParser.class);
    static InputStream is = null;
    static String jsonString = "";
    String charset = "UTF-8";
    HttpURLConnection conn;
    DataOutputStream wr;
    StringBuilder result;
    URL urlObj;
    JSONObject jObj = null;
    JSONArray jsonArray = null;
    StringBuilder sbParams;
    String paramsString;
    int i = 0;
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    Statement statement = null;

    public JSONArray makeHttpRequest(String url, String method,
                                     HashMap<String, String> params) {

        sbParams = new StringBuilder();
        int i = 0;
        if (params != null) {
            for (String key : params.keySet()) {
                try {
                    if (i != 0) {
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), charset));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }

        if (method.equals("POST")) {
            // request method is POST
            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(20000);

                conn.setConnectTimeout(15000);

                conn.connect();

                paramsString = sbParams.toString();

                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(paramsString);
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (method.equals("GET")) {
            // request method is GET

            if (sbParams.length() != 0) {
                url += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(false);

                conn.setRequestMethod("GET");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(20000);

                conn.setConnectTimeout(15000);

                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            //Receive the response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            result = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            LOG.info("JSON Parser", "result: " + result.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();

        // try parse the string to a JSON object
        try {
            jsonArray = new JSONArray(result.toString());
        } catch (JSONException e) {
            LOG.info("JSON Parser", "Error parsing data " + e.toString());
        } catch (NullPointerException np) {
            LOG.info("JSON Parser", "No response from server.");
        }

        // return JSON Object
        return jsonArray;
    }
}