package tr.com.dizivefilm.ws.pages;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by ozcan on 14.10.2015.
 */
@ApplicationPath("service")
public class Base extends Application {
}
