package tr.com.dizivefilm.ws.services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tr.com.dizivefilm.ws.helpers.ServerConnection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ozcan on 14.10.2015.
 */
@Path("/users")
public class Users {

    static final Logger LOG = LoggerFactory.getLogger(Users.class);
    ServerConnection serverConnection = new ServerConnection();

    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(@FormParam("first_name") String firstName,
                                 @FormParam("last_name") String lastName,
                                 @FormParam("password") String password,
                                 @FormParam("email") String email) {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONObject jsonObject = new JSONObject();

        LOG.info("-------------------------");
        LOG.info("Register request");
        LOG.info("Name: " + firstName);
        LOG.info("Surname: " + lastName);
        LOG.info("Email: " + email);
        LOG.info("-------------------------");

        String registerSql = "INSERT INTO Users (first_name, last_name, password, email) VALUES (?,?,?,?)";

        try {
            preparedStatement = connection.prepareStatement(registerSql);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, email);
            preparedStatement.executeUpdate();
            success = true;
            message = "Kayıt başarıyla gerçekleşti";
            LOG.info("User registration is successful");
            LOG.info("-------------------------");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            if (e.getMessage().contains("Duplicate entry"))
                message = "Bu email ile başka kayıtlı bir kullanıcı var, başka bir email deneyiniz.";
            else
                message = e.getMessage();
        } finally {
            serverConnection.close();
        }

        jsonObject.put("success", success);
        jsonObject.put("message", message);

        return Response.status(Response.Status.OK).entity(jsonObject.toString()).build();
    }

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@FormParam("email") String email, @FormParam("password") String password) {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONObject jsonObject = new JSONObject();
        String firstName = null;
        String lastName = null;
        String loginSql = "SELECT * FROM Users where email = ? AND password = ?";

        LOG.info("-------------------------");
        LOG.info("User login request");
        LOG.info("Email: " + email);
        LOG.info("Password: " + password);
        LOG.info("-------------------------");

        try {
            preparedStatement = connection.prepareStatement(loginSql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                firstName = resultSet.getString("first_name");
                lastName = resultSet.getString("last_name");
                success = true;
                message = "Kullanıcı girişi başarılı";
                LOG.info("User login is succesful");
                LOG.info("-------------------------");
            }


        } catch (SQLException e) {
            LOG.error(e.getMessage());
            message = e.getMessage();
        } finally {
            serverConnection.close();
        }

        if (!success) {
            message = "Böyle bir kullanıcı bulunmamaktadır.";
        }

        jsonObject.put("success", success);
        jsonObject.put("message", message);
        jsonObject.put("user_details", new JSONObject().put("first_name", firstName).put("last_name", lastName));

        return Response.status(Response.Status.OK).entity(jsonObject.toString()).build();
    }


    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String query = "SELECT * FROM Users";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;

        try {
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("user_id", resultSet.getString("user_id"));
                jsonObject.put("first_name", resultSet.getString("first_name"));
                jsonObject.put("last_name", resultSet.getString("last_name"));
                jsonObject.put("email", resultSet.getString("email"));
                jsonArray.put(jsonObject);
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            serverConnection.close();
        }
        LOG.info(jsonArray.toString());
        return Response.status(Response.Status.OK).entity(jsonArray.toString()).build();
    }

    @POST
    @Path("/change-password")
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(@FormParam("email") String email,
                                   @FormParam("old_password") String oldPassword,
                                   @FormParam("new_password") String newPassword) {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String getOldPasswordSql = "SELECT password FROM Users WHERE email = ?";
        String existingPassword = "";
        String updatePasswordSql = "UPDATE Users SET password = ? WHERE email = ?";
        Boolean success = false;
        String message = "";

        LOG.info("-------------------------");
        LOG.info("Change user password request");
        LOG.info("Email: " + email);
        LOG.info("Old Password: " + oldPassword);
        LOG.info("New Password: " + newPassword);
        LOG.info("-------------------------");

        try {
            preparedStatement = connection.prepareStatement(getOldPasswordSql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                existingPassword = resultSet.getString("password");
                LOG.info("Password in database: " + existingPassword);
            }

            if (oldPassword.equals(existingPassword)) {
                LOG.info("Password in database matched with given old password.");
                LOG.info("Changing old password with new one.");
                preparedStatement = connection.prepareStatement(updatePasswordSql);
                preparedStatement.setString(1, newPassword);
                preparedStatement.setString(2, email);
                preparedStatement.executeUpdate();
                success = true;
                message = "Şifre başarıyla güncellendi.";
                LOG.info("Changing user password is successful");
                LOG.info("-------------------------");
            } else {
                message = "Yanlış şifre";
                LOG.info("Password in database didn't match with given old password.");
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage());
            message = e.getMessage();
        } finally {
            serverConnection.close();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", success);
        jsonObject.put("message", message);

        return Response.status(Response.Status.OK).entity(jsonObject.toString()).build();
    }
}
