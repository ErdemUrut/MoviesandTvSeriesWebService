package tr.com.dizivefilm.ws.services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tr.com.dizivefilm.ws.helpers.ServerConnection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ozcan on 15.10.2015.
 */
@Path("/movies")
public class Movies {
    static final Logger LOG = LoggerFactory.getLogger(Movies.class);
    ServerConnection serverConnection = new ServerConnection();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMovies() {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;
        JSONObject responseJsonObject = null;
        String moviesSql = "SELECT * FROM Movies";


        try {
            preparedStatement = connection.prepareStatement(moviesSql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("movies_id", resultSet.getString("movies_id"));
                jsonObject.put("title", resultSet.getString("title"));
                jsonObject.put("ranking", resultSet.getString("ranking"));
                jsonObject.put("rating", resultSet.getString("rating"));
                jsonObject.put("url", resultSet.getString("url"));
                jsonObject.put("year", resultSet.getString("year"));
                jsonArray.put(jsonObject);
            }

            success = true;
            message = "İşlem tamamlandı";
        } catch (SQLException sqlEx) {
            LOG.error(sqlEx.getMessage());
            message = sqlEx.getMessage();
        } catch (JSONException jsonEx) {
            LOG.error(jsonEx.getMessage());
            message = jsonEx.getMessage();
        } finally {
            serverConnection.close();
        }

        responseJsonObject = new JSONObject();
        responseJsonObject.put("success", success);
        responseJsonObject.put("message", message);
        responseJsonObject.put("movies", jsonArray);

        return Response.status(Response.Status.OK).entity(responseJsonObject.toString()).build();
    }

    @POST
    @Path("/favorites/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addFavorites(@FormParam("movies_ids") String moviesIds, @FormParam("email") String email) {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONObject responseJsonObject = null;

        String moviesIdsArray[] = moviesIds.split(",");

        String query = "INSERT INTO FavoriteMovies (user_id,movies_id) VALUES ((SELECT user_id from Users where email=?),?)";


        try {
            for (int i = 0; i < moviesIdsArray.length; i++) {

                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, email);
                preparedStatement.setInt(2, Integer.parseInt(moviesIdsArray[i]));
                preparedStatement.executeUpdate();


            }

            success = true;
            message = "Başarıyla eklendi.";

        } catch (SQLException sqlEx) {
            LOG.error(sqlEx.getMessage());
            message = sqlEx.getMessage();
        } finally {
            serverConnection.close();
        }
        responseJsonObject = new JSONObject();
        responseJsonObject.put("success", success);
        responseJsonObject.put("message", message);

        return Response.status(Response.Status.OK).entity(responseJsonObject.toString()).build();
    }

    @GET
    @Path("/favorites/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFavoriteMovies() {
        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;
        JSONObject responseJsonObject = null;
        String favoriteMoviesSql = "select title from Movies where movies_id in (select movies_id from FavoriteMovies)";

        try {
            preparedStatement = connection.prepareStatement(favoriteMoviesSql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                jsonArray.put(new JSONObject()
                        .put("title", resultSet.getString("title")));

            }
            success = true;
            message = "İşlem tamamlandı";

        } catch (SQLException sqlEx) {
            LOG.error(sqlEx.getMessage());
            message = sqlEx.getMessage();
        } catch (JSONException jsonEx) {
            LOG.error(jsonEx.getMessage());
            message = jsonEx.getMessage();
        } finally {
            serverConnection.close();
        }

        responseJsonObject = new JSONObject();
        responseJsonObject.put("success", success);
        responseJsonObject.put("message", message);
        responseJsonObject.put("favoritemovies", jsonArray);

        return Response.status(Response.Status.OK).entity(responseJsonObject.toString()).build();

    }
}
