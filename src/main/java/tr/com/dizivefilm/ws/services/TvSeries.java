package tr.com.dizivefilm.ws.services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tr.com.dizivefilm.ws.helpers.ServerConnection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ozcan on 14.10.2015.
 */
@Path("/tvseries")
public class TvSeries {
    static final Logger LOG = LoggerFactory.getLogger(Movies.class);
    ServerConnection serverConnection = new ServerConnection();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTvSeries() {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONArray jsonArray = new JSONArray();
        JSONObject responseJsonObject = null;
        String tvSeriesSql = "SELECT * FROM TvSeries";


        try {
            preparedStatement = connection.prepareStatement(tvSeriesSql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                jsonArray.put(new JSONObject()
                        .put("tvseries_id", resultSet.getString("tvseries_id"))
                        .put("name", resultSet.getString("name"))
                        .put("type", resultSet.getString("type"))
                        .put("language", resultSet.getString("language"))
                        .put("genres", resultSet.getString("genres"))
                        .put("status", resultSet.getString("status"))
                        .put("runtime", resultSet.getString("runtime"))
                        .put("rating", resultSet.getString("rating")));
            }

            success = true;
            message = "İşlem tamamlandı";
        } catch (SQLException sqlEx) {
            LOG.error(sqlEx.getMessage());
            message = sqlEx.getMessage();
        } catch (JSONException jsonEx) {
            LOG.error(jsonEx.getMessage());
            message = jsonEx.getMessage();
        } finally {
            serverConnection.close();
        }

        responseJsonObject = new JSONObject();
        responseJsonObject.put("success", success);
        responseJsonObject.put("message", message);
        responseJsonObject.put("tvseries", jsonArray);

        return Response.status(Response.Status.OK).entity(responseJsonObject.toString()).build();
    }

    @POST
    @Path("/favorites/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addToFavorites(@FormParam("email") String email,
                                   @FormParam("tvseries_ids") String tvSeriesIds) {


        String[] tvSeriesIdsArray = tvSeriesIds.split(",");
        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONObject jsonObject = null;
        String insertFavoriteTvSeriesSql = "INSERT INTO FavoriteTvSeries (user_id, tvseries_id) VALUES ((SELECT user_id FROM Users WHERE email = ?),?)";


        LOG.info("-------------------------");
        LOG.info("Add to favorites request");
        LOG.info("Email: " + email);
        LOG.info("Tv series: " + tvSeriesIds);
        LOG.info("-------------------------");
        try {
            preparedStatement = connection.prepareStatement(insertFavoriteTvSeriesSql);

            for (int i = 0; i < tvSeriesIdsArray.length; i++) {
                preparedStatement.setString(1, email);
                preparedStatement.setString(2, tvSeriesIdsArray[i]);
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
            success = true;
            message = "Diziler favorilerinize eklendi.";
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            message = e.getMessage();
        } finally {
            serverConnection.close();
        }

        jsonObject = new JSONObject();
        jsonObject.put("success", success);
        jsonObject.put("message", message);

        return Response.status(Response.Status.OK).entity(jsonObject.toString()).build();
    }

    @POST
    @Path("/favorites")
    @Produces(MediaType.APPLICATION_JSON)
    public Response usersFavorites(@FormParam("email") String email) {

        Connection connection = serverConnection.getMySqlConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean success = false;
        String message = "Bilinmeyen bir hata";
        JSONObject jsonObject = null;
        JSONArray seriesJSONArray = new JSONArray();
        String usersFavoritesSql = "SELECT * FROM TvSeries WHERE tvseries_id IN (SELECT tvseries_id FROM FavoriteTvSeries WHERE user_id IN (SELECT user_id FROM Users WHERE email = ?))";

        try {
            preparedStatement = connection.prepareStatement(usersFavoritesSql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                seriesJSONArray.put(new JSONObject()
                        .put("name", resultSet.getString("name"))
                        .put("type", resultSet.getString("type"))
                        .put("language", resultSet.getString("language"))
                        .put("genres", resultSet.getString("genres"))
                        .put("status", resultSet.getString("status"))
                        .put("runtime", resultSet.getString("runtime"))
                        .put("rating", resultSet.getString("rating"))
                );
            }

            success = true;
            message = "İşlem başarılı";
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            message = e.getMessage();
        } finally {
            serverConnection.close();
        }

        jsonObject = new JSONObject();
        jsonObject.put("success", success);
        jsonObject.put("message", message);
        jsonObject.put("user", email);
        jsonObject.put("favorite_tvseries", seriesJSONArray);

        return Response.status(Response.Status.OK).entity(jsonObject.toString()).build();
    }
}
