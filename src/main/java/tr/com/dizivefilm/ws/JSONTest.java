package tr.com.dizivefilm.ws;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by ozcan on 24.10.2015.
 */
@Path("/json-test")
public class JSONTest {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response testJson() {
        JSONObject mainJsonObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        JSONObject jsonObject3 = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArray1 = new JSONArray();

        jsonObject.put("test", "test");
        jsonObject.put("test1", "test1");
        jsonObject1.put("test", "test");
        jsonObject1.put("test1", "test1");
        jsonArray.put(jsonObject);
        jsonArray.put(jsonObject1);
        jsonObject2.put("testArray", jsonArray);
        jsonArray1.put(jsonObject2);
        jsonObject3.put("testArray1", jsonArray1);
        mainJsonObject.put("main", jsonObject3);


        return Response.status(Response.Status.OK).entity(mainJsonObject.toString()).build();
    }
}